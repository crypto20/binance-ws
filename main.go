package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"net/url"
	"os"
	"strconv"

	"github.com/gorilla/websocket"
	"github.com/segmentio/kafka-go"
	log "github.com/sirupsen/logrus"
)

// TradeEvent event information
type TradeEvent struct {
	Event       string  `json:"e"`
	EventTime   int64   `json:"E"`
	Symbol      string  `json:"s"`
	TradeID     int64   `json:"t"`
	Price       float64 `json:"p,string"`
	Quantity    float64 `json:"q,string"`
	BuyerID     int64   `json:"b"`
	SellerID    int64   `json:"a"`
	TradeTime   int64   `json:"T"`
	MarketMaker bool    `json:"m"`
}

var baseAddr = flag.String("addr", "stream.binance.com:9443", "http service address")

func getEnvOr(key string, or string) string {
	val := os.Getenv(key)
	if len(val) < 1 {
		return or
	}
	return val
}

func main() {
	kafkaURL := getEnvOr("BINANCE_API_KAFKA_URL", "localhost:9092")
	tradeSymbol := getEnvOr("BINANCE_API_TRADE_SYMBOL", "eoseur")
	msgBufSize, err := strconv.ParseInt(getEnvOr("BINANCE_API_MSG_BUF_SIZE", "200"), 10, 32)
	if err != nil {
		panic(err)
	}
	env := getEnvOr("BINANCE_API_ENV", "development")

	switch env {
	case "development":
		log.SetLevel(log.DebugLevel)
	case "production":
		log.SetLevel(log.InfoLevel)
	}

	log.Infof("Create Writer for kafka %s and topic: trade-%s...", kafkaURL, tradeSymbol)
	w := kafka.NewWriter(kafka.WriterConfig{
		Brokers: []string{kafkaURL},
		Topic:   fmt.Sprintf("trade-%s", tradeSymbol),
		Async:   true,
	})
	defer w.Close()

	log.Infof("Receive buffer with size: %d.", msgBufSize)
	tradeChan := make(chan TradeEvent, msgBufSize)

	go func() {
		url := url.URL{Scheme: "wss", Host: *baseAddr, Path: fmt.Sprintf("/ws/%s@trade", tradeSymbol)}
		log.Infof("Connection to %s...", url.String())
		conn, _, err := websocket.DefaultDialer.Dial(url.String(), nil)
		if err != nil {
			panic(err)
		}
		defer conn.Close()
		for {
			_, msg, err := conn.ReadMessage()
			if err != nil {
				log.Println("read: ", err)
				continue
			}
			var trade TradeEvent
			err = json.Unmarshal(msg, &trade)
			if err != nil {
				log.Warn("unmarshal: ", err)
				continue
			}
			tradeChan <- trade
		}
	}()

	for {
		trade := <-tradeChan
		log.Debug("write to topic: ", trade)
		key := fmt.Sprintf("%d", trade.EventTime)
		tradeStr := fmt.Sprintf(
			"%s,%d,%s,%d,%f,%f,%d,%d,%d,%t",
			trade.Event,
			trade.EventTime,
			trade.Symbol,
			trade.TradeID,
			trade.Price,
			trade.Quantity,
			trade.BuyerID,
			trade.SellerID,
			trade.TradeTime,
			trade.MarketMaker)
		if err := w.WriteMessages(context.Background(), kafka.Message{
			Key:   []byte(key),
			Value: []byte(tradeStr),
		}); err != nil {
			panic(err)
		}
	}
}
